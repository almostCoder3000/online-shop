import styled from 'styled-components';

export const ProductListContainer = styled.div`
    padding: 10px 20px;
    width: 100%;
    max-width: 808px;
    margin: 0 auto;
`;

export const Row = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin: 0 -6px;
`;

export const Col = styled.div`
    padding: 0 6px 15px;
    width: 50%;
    max-width: 340px;

    @media only screen and (min-width: 768px) {
        width: 33.33%;
    }
`;
