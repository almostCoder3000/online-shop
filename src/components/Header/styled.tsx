import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { OrangeCounterStyles } from '../../ui';

export const HeaderContainer = styled.header`
    padding: 20px 20px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media only screen and (min-width: 808px) {
        padding: 20px calc(50% - 384px);
    }
`;

export const Logo = styled(Link)`
    font-size: 24px;
    line-height: 33px;
    font-family: 'Open Sans Bold';
    display: flex;
    flex-direction: row;
    align-items: center;
    position: relative;

    &::before {
        content: '';
        display: block;
        width: 24px;
        height: 24px;
        margin-right: 5px;
        background: url(${require('../../assets/icons/logo.svg')}) 0 0 no-repeat;
    }
`;

export const Cart = styled(Link)<{ count: number }>`
    height: 50px;
    background-color: #ededed;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 0 15px;
    position: relative;
    border-radius: 10px;
    transition: min-width 0.05s;
    min-width: ${(props) => (props.count > 0 ? '159px' : '0px')};

    span {
        font-family: 'Open Sans Bold';
        margin-left: 5px;
    }

    &::after {
        content: '${(props) => props.count}';
        display: ${(props) => (props.count > 0 ? 'block' : 'none')};
        ${OrangeCounterStyles}
    }

    &::before {
        content: '';
        display: block;
        width: 24px;
        height: 24px;
        background: url(${require('../../assets/icons/cart.svg')}) 0 0 no-repeat;
        margin-right: ${(props) => (props.count > 0 ? '8px' : '0px')};
    }
`;
