import * as React from 'react';
import { useSelector } from 'react-redux';
import { AppState } from '../../store';
import { OrderMap } from '../../store/shop/types';
import { HeaderContainer, Logo, Cart } from './styled';

const Header: React.FC = () => {
    const cart = useSelector<AppState, OrderMap>((state) => state.shop.cart);

    const total: number = cart.getTotal();
    return (
        <HeaderContainer>
            <Logo to="/">Shop</Logo>
            <Cart to="/cart" count={cart.getOrderItemsSum()}>
                {total > 0 && (
                    <>
                        Total: <span>${total}</span>
                    </>
                )}
            </Cart>
        </HeaderContainer>
    );
};

export default Header;
