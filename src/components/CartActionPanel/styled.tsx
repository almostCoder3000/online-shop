import styled from 'styled-components';
import { Button } from '../../ui';

export const PanelContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 20px;
    max-width: 808px;
    margin: 0 auto;

    section {
        display: flex;
        flex-direction: row;
    }
`;

export const OrangeButton = styled(Button)`
    background-color: #ff7c50;
    width: 100px;

    &:hover {
        background-color: #ff7040;
    }
`;

export const Title = styled.div`
    font-size: 24px;
    line-height: 33px;
    display: flex;
    flex-direction: row;

    &::before {
        content: '';
        width: 24px;
        height: 24px;
        margin-right: 8px;
        background: url(${require('../../assets/icons/list.svg')}) 0 0 no-repeat;
    }
`;
