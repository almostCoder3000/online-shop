import * as React from 'react';
import configure from '../../configure';
import { IProduct } from '../../store/shop/types';
import { Button, DeleteButton } from '../../ui';
import { Actions, CartItemContainer, Info } from './styled';

interface ICartitemProps extends IProduct {
    count: number;
    onIncrement: () => void;
    onDecrement: () => void;
    onRemove: () => void;
}

const Cartitem: React.FunctionComponent<ICartitemProps> = ({
    image,
    name,
    price,
    count,
    onIncrement,
    onDecrement,
    onRemove,
}) => {
    return (
        <CartItemContainer>
            <Info>
                <img src={configure.urlServer + image} alt="" />
                <div>
                    <p>{name}</p>
                    <p>${price}</p>
                </div>
            </Info>
            <Actions>
                <Button onClick={onDecrement}>-</Button>
                <span>{count}</span>
                <Button onClick={onIncrement}>+</Button>
            </Actions>
            <DeleteButton onClick={onRemove} />
        </CartItemContainer>
    );
};

function areEqual(prevProps: ICartitemProps, nextProps: ICartitemProps) {
    return prevProps.name === nextProps.name && prevProps.count === nextProps.count;
}

export default React.memo(Cartitem, areEqual);
