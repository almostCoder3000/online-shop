import {
    START_FETCH,
    FETCH_ERROR,
    StartFetchAction,
    ShopActionTypes,
    GET_PRODUCTS,
    ADD_PRODUCT_ITEM_TO_CART,
    LOAD_STORAGE_DATA,
    OrderMap,
    REMOVE_PRODUCT_ITEM_FROM_CART,
    DELETE_PRODUCT_FROM_CART,
    CLEAR_CART,
    IProduct,
} from './types';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { getRequest, FetchError } from '../../helpers';

type ThunkAct = ThunkAction<Promise<void>, {}, {}, AnyAction>;
type ThunkDisp = ThunkDispatch<{}, {}, AnyAction>;

export const getProducts = (dealers?: string[]): ThunkAct => async (dispatch: ThunkDisp) => {
    fetchWrapper(dispatch, async () => {
        const res = await getRequest('/api/goods/', { dealers: dealers ? dealers.join(',') : '' });
        if (res.status !== 200) {
            throw new FetchError(res.status, res.data.error);
        }
        const products: IProduct[] = res.data;
        dispatch({ type: GET_PRODUCTS, payload: products });

        const cartJSON: string | null = localStorage.getItem('cart');

        if (cartJSON) {
            const loadedCart: OrderMap = new OrderMap(Array.from(JSON.parse(cartJSON)));
            dispatch({ type: LOAD_STORAGE_DATA, payload: loadedCart });
        }
    });
};

export const addProductItemToCart = (key: string): ThunkAct => async (dispatch: ThunkDisp) => {
    dispatch({ type: ADD_PRODUCT_ITEM_TO_CART, payload: key });
};

export const removeProductItemFromCart = (key: string): ThunkAct => async (dispatch: ThunkDisp) => {
    dispatch({ type: REMOVE_PRODUCT_ITEM_FROM_CART, payload: key });
};

export const deleteProductFromCart = (key: string): ThunkAct => async (dispatch: ThunkDisp) => {
    dispatch({ type: DELETE_PRODUCT_FROM_CART, payload: key });
};

export const clearCart = (): ThunkAct => async (dispatch: ThunkDisp) => {
    dispatch({ type: CLEAR_CART });
};

const fetchWrapper = async (dispatch: ThunkDisp, mainAction: () => Promise<void>) => {
    const fetching: StartFetchAction = { type: START_FETCH };
    dispatch(fetching);

    try {
        await mainAction();
    } catch (e) {
        const error: ShopActionTypes = { type: FETCH_ERROR, payload: e };
        dispatch(error);
    }
};
