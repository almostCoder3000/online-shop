import {
    START_FETCH,
    FETCH_ERROR,
    ShopActionTypes,
    ShopState,
    GET_PRODUCTS,
    GET_DEALERS,
    ADD_PRODUCT_ITEM_TO_CART,
    REMOVE_PRODUCT_ITEM_FROM_CART,
    OrderMap,
    DELETE_PRODUCT_FROM_CART,
    LOAD_STORAGE_DATA,
    CLEAR_CART,
} from './types';

const initialState: ShopState = {
    fetching: false,
    products: new Map(),
    dealers: [],
    cart: new OrderMap(),
    error: undefined,
};

export function shopReducer(state = initialState, action: ShopActionTypes): ShopState {
    switch (action.type) {
        case START_FETCH:
            return {
                ...state,
                fetching: true,
                error: undefined,
            };

        case LOAD_STORAGE_DATA:
            return {
                ...state,
                cart: action.payload.clearTrashProducts(state.products),
            };

        case CLEAR_CART:
            return {
                ...state,
                cart: state.cart.clearCart(),
            };

        case GET_PRODUCTS:
            return {
                ...state,
                fetching: false,
                products: new Map(action.payload.map((prod) => [`${prod.name}|$|${prod.price}`, prod])),
            };

        case ADD_PRODUCT_ITEM_TO_CART:
            return {
                ...state,
                fetching: false,
                cart: state.cart.addItem(action.payload),
            };

        case REMOVE_PRODUCT_ITEM_FROM_CART:
            return {
                ...state,
                cart: state.cart.removeItem(action.payload),
            };

        case DELETE_PRODUCT_FROM_CART:
            return {
                ...state,
                cart: state.cart.clearItem(action.payload),
            };

        case GET_DEALERS:
            return {
                ...state,
                fetching: false,
                dealers: action.payload,
            };

        case FETCH_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload,
            };

        default:
            return state;
    }
}
