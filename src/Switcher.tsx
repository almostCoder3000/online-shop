import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import CartPage from './pages/CartPage';
import MainPage from './pages/MainPage';
import { getProducts } from './store/shop/actions';

export default function Switcher() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProducts(['0c4aab30', '1efa7e46', '86e64a33']));
    }, [dispatch]);

    return (
        <React.Fragment>
            <Header />
            <Switch>
                <Route path="/cart">
                    <CartPage />
                </Route>
                <Route path="/">
                    <MainPage />
                </Route>
            </Switch>
        </React.Fragment>
    );
}
