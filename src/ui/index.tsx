import styled, { createGlobalStyle, css } from 'styled-components';

export const GlobalStyle = createGlobalStyle`

    body {
        margin: 0;
    }
    html {
        scroll-behavior: smooth;
    }
    * {
        box-sizing: border-box;
        color: #333333;
        font-family: "Open Sans Regular";
        outline: none;
    }
    #root>div {position: relative;}

    a {
        text-decoration: none;
    }

    p {
        font-size: 18px;
        line-height: 25px;
        margin: 0;
    }
`;

export const Button = styled.button`
    min-width: 40px;
    height: 40px;
    color: #fff;
    border-radius: 10px;
    border: none;
    background-color: #333;
    font-size: 16px;
    cursor: pointer;
    transition: background-color 0.3s;
    font-family: 'Open Sans Regular';
    -webkit-tap-highlight-color: transparent;

    &:hover {
        background-color: #000;
    }
`;

export const OrangeCounterStyles = css`
    text-align: center;
    line-height: 26px;
    font-size: 14px;
    width: 30px;
    height: 30px;
    background-color: #ff7c50;
    color: #fff;
    border: 2px solid #fff;
    border-radius: 50%;
    position: absolute;
    top: -13px;
    right: -13px;
    box-sizing: border-box;
`;

export const DeleteButton = styled.div`
    background: #ededed;
    border-radius: 10px;
    height: 40px;
    width: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: 10px;

    &::after {
        content: '';
        display: block;
        width: 24px;
        height: 24px;
        background: url(${require('../assets/icons/trash.svg')}) 0 0 no-repeat;
    }
`;
