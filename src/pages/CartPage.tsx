import React from 'react';
import CartActionPanel from '../components/CartActionPanel';
import CartItemList from '../components/CartItemList';

const CartPage: React.FC = () => {
    return (
        <React.Fragment>
            <CartActionPanel />
            <CartItemList />
        </React.Fragment>
    );
};

export default CartPage;
