import React from 'react';
import ProductList from '../components/ProductList';

interface Props {}

const MainPage: React.FC<Props> = () => {
    return (
        <React.Fragment>
            <ProductList />
        </React.Fragment>
    );
};

export default MainPage;
